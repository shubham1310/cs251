% Using make files
% Piyush P Kurur
% January 19, 2015

#

## What is involved in building?

- Code is spread into multiple files.
- The final executable has multiple dependencies
- They can have indirect dependencies.
- Need to build when things change
- Build only when things actually change.

## Main idea

- User creates a Makefile that mentions dependencies
- The make program reads these dependencies and does required action.

## Small example

```Makefile


all : hello

hello: hello.c
	gcc -o hello hello.c

clean:
	rm hello

```

## You can use variables.

```Makefile
CC=gcc

...

hello: hello.c
	${CC} -o hello hello.c

```

## Stuff that you can do with variables

- Can be set at the command line `make FOO=bar target`
- Can be given default values.
- Can be used in conditionals.
- Many more stuff.

## You can use patterns for compilation

```Makefile

%.html: %.md tweak.css
	${PANDOC} -s $< -o $@

```

## Phony targets

```Makefile

.PHONY: all clean
all:

clean:

```

## Building subdirectories.

```bash

make -C path/to/directory target

```
